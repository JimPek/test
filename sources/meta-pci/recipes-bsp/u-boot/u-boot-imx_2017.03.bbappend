FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
FILESPATH_prepend := "/home/et/imx8-pci-004/sources/meta-pci/recipes-bsp/u-boot/u-boot-imx/oe-local-files:"
#FILESPATH_prepend := "${THISDIR}/u-boot-imx/oe-local-files:"



inherit externalsrc
# NOTE: We use pn- overrides here to avoid affecting multiple variants in the case where the recipe uses BBCLASSEXTEND
EXTERNALSRC_pn-u-boot-imx = "/home/et/imx8-pci-004/sources/meta-pci/recipes-bsp/u-boot/u-boot-imx"
#EXTERNALSRC_pn-u-boot-imx = "${THISDIR}/u-boot-imx"

# initial_rev: 25375229c5b40312604c4e21203b9a99ad8b52a6
