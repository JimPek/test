FILESEXTRAPATHS_prepend := "${THISDIR}/${PN}:"
FILESPATH_prepend := "/home/et/imx8-pci-004/sources/meta-pci/recipes-bsp/linux/linux-imx/oe-local-files:"

inherit externalsrc
# NOTE: We use pn- overrides here to avoid affecting multiple variants in the case where the recipe uses BBCLASSEXTEND
EXTERNALSRC_pn-linux-imx = "/home/et/imx8-pci-004/sources/meta-pci/recipes-bsp/linux/linux-imx"
SRCTREECOVEREDTASKS = "do_validate_branches do_kernel_checkout do_fetch do_unpack do_patch do_kernel_configme do_kernel_configcheck"

do_configure_append() {
    cp ${B}/.config ${S}/.config.baseline
    ln -sfT ${B}/.config ${S}/.config.new
}

# initial_rev: 6df74740bec41bc2e17fc76c20f741401985c808
# commit: 61f698944c1d77c89813d99e15f004c8e1034719
