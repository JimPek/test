require imx-gpu-viv-v6.inc

SRC_URI[md5sum] = "a3d0622a8f90dca2d61725cb6f9ea823"
SRC_URI[sha256sum] = "b610fddddc87238d93fb0e8908d0151f0274abd355744167469b81f5bd593a24"

COMPATIBLE_MACHINE = "(mx6q|mx6dl|mx6sx|mx6sl|mx7ulp)"
